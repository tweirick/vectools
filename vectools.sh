_vectools()
{
  local cur prev opts
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD-1]}"
  opts="--help $(vectools.py --commands?)"
#  echo $opts
#  if [[ ${cur} == -* ]]; then
  COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
  return 0
# fi
}
complete -F _vectools vectools.py

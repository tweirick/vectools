.. vectortools documentation master file, created by
   sphinx-quickstart on Fri Aug 14 16:04:28 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to vectortools's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 4

   vectortools


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


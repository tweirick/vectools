# Created by raphael at 18.07.16
Feature: unique
  makes rows unique, does not sort it. Only apply on works

  @unique
  Scenario: run a unique test
    Given the file tempfile containing
    """
    1 0 1 0 1
    1 0 1 0 1
    0 1 0 1 0
    0 1 0 1 0
    0 1 0 1 0
    0 1 0 1 0
    2 0 1 0 1
    2 0 1 0 1
    2 0 1 0 1
    0 2 0 1 0
    0 2 0 1 0
    0 2 0 1 0
    0 2 0 1 0
    0 2 0 1 0
    0 2 0 1 0
    0 2 0 1 0
    0 2 0 1 0
    0 2 0 1 0
    0 2 0 1 0
    0 2 0 1 0
    2 0 2 0 1
    2 0 2 0 1
    """
    Given file tempfile as parameter
    When we run unique from manipulation with tmpfile
    Then we expect the matrix
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """

  @unique
  Scenario: has column titles and ignore row titles
    Given the file test containing
    """
    id,1,2
    id,1,2
    b,1,2
    id,1,2
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    When we run unique from manipulation with tmpfile
    Then we expect the matrix
    """
    id,1,2
    id,1,2
    """


  @unique
  Scenario: has column titles, row titles are included in the unique process
    Given the file test containing
    """
    id,1,2
    id,1,2
    b,1,2
    id,1,2
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    When we run unique from manipulation with tmpfile
    Then we expect the matrix
    """
    id,1,2
    id,1,2
    b,1,2
    """

  @unique
  Scenario: normal unique process
    Given the file test containing
    """
    id,1,2
    id,1,2
    b,1,2
    id,1,2
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    When we run unique from manipulation with tmpfile
    Then we expect the matrix
    """
    id,1,2
    b,1,2
    """

  @unique
  Scenario: apply unique only on column1 to get only transcript
    Given the file cuff containing
    """
    gene_id,feature,control,tnfa,vegf
    ENSG1234,transcript,0,1,2
    ENSG1234,exon,0.1,2,5
    ENSG1234,exon,3,6,5
    ENSG1234,exon,1,2,5
    ENSG1234,exon,1,3,7
    ENSG81234,transcript,0,1,2
    ENSG81234,exon,0.1,2,5
    ENSG81234,exon,3,6,5
    ENSG81234,exon,1,2,5
    ENSG81234,exon,1,3,7
    """
    Given file cuff as parameter
    Given parameter --delimiter = ,
    Given last parameter -c
    Given parameter --only-apply-on = 0
    When we run unique from manipulation with tmpfile
    Then we expect the matrix
    """
    gene_id,feature,control,tnfa,vegf
    ENSG1234,transcript,0,1,2
    ENSG81234,transcript,0,1,2
    """

  @unique
  Scenario: reverse unique. print only those, where columns 1 and 2 appear more than once
    Given the file cuff containing
    """
    gene_id,feature,control,tnfa,vegf
    ENSG1234,transcript,0,1,2
    ENSG1234,exon,0.1,2,5
    ENSG1234,exon,3,6,5
    ENSG1234,exon,1,2,5
    ENSG1234,exon,1,3,7
    ENSG81234,transcript,0,1,2
    ENSG81234,exon,0.1,2,5
    ENSG81234,exon,3,6,5
    ENSG81234,exon,1,2,5
    ENSG81234,exon,1,3,7
    """
    Given file cuff as parameter
    Given parameter --delimiter = ,
    Given last parameter -c
    Given parameter --only-apply-on = 0,1
    Given last parameter --reverse
    When we run unique from manipulation with tmpfile
    Then we expect the matrix
    """
    gene_id,feature,control,tnfa,vegf
    ENSG1234,exon,0.1,2,5
    ENSG81234,exon,0.1,2,5
    """
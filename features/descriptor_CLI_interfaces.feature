# Created by tyler at 27/04/16
Feature: #Enter feature name here
  # Enter feature description here

#  @pending
#  Scenario: run a simple test for ncomposition
#    Given a fasta file with equal counts of all amino acids.
#      """
#      >AllAminoAcids
#      ARNDCEQGHILKMFPSTWYVARNDCEQGHILKMFPSTWYVARNDCEQGHILKMFPSTWYVARNDCEQGHILKMFPSTWYV
#      """
#    When running ncomposition with all defaults (kmer length is 1 and standard fasta format.).
#    Then there should be a 20 element vector with each element equaling 0.05.


# Created by Tyler at 11.07.16
Feature: Test generating ncomp descriptors

    @ncomp_aminoacid_default_parameters_single_fasta
    Scenario: default parameters ncomp
        Given the file file_a containing
        """
        >OneHundredAlanines
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAA
        """
        Given file file_a as parameter
        When we run ncomposition_command_line from descriptor_CLI_interfaces
        Then we expect the matrix
        """
        1.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
        """

    @ncomp_aminoacid_default_parameters_multifasta_input
    Scenario: default parameters ncomp
        Given the file file_a containing
        """
        >OneHundredAlanines
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAA
        >TwoHundredCytosines
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        >OneHundredAlanines_TwoHundredCytosines
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAACCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        """
        Given file file_a as parameter
        When we run ncomposition_command_line from descriptor_CLI_interfaces
        Then we expect the matrix
        """
        1.0  0.0  0.0  0.0  0.0  0.0 0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
        0.0  1.0  0.0  0.0  0.0  0.0 0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
        0.33333 0.66667  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
        """

    @ncomp_aminoacid_single_fasta_with_row_titles
    Scenario: aacomp with row titles
        Given the file file_a containing
        """
        >OneHundredAlanines
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAA
        """
        Given file file_a as parameter
        Given last parameter --row-titles
        When we run ncomposition_command_line from descriptor_CLI_interfaces
        Then we expect the matrix
        """
        OneHundredAlanines 1.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
        """

    @ncomp_aminoacid_with_column_titles
    Scenario: default parameters ncomp
        Given the file file_a containing
        """
        >OneHundredAlanines
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAA
        """
        Given file file_a as parameter
        Given last parameter --column-titles
        When we run ncomposition_command_line from descriptor_CLI_interfaces
        Then we expect the matrix
        """
        A C D E F G H I K L M N P Q R S T V W Y
        1.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
        """

    @ncomp_aminoacid_delimiter_check
    Scenario: default parameters ncomp
        Given the file file_a containing
        """
        >OneHundredAlanines
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAA
        """
        Given parameter --delimiter = ,
        Given file file_a as parameter
        When we run ncomposition_command_line from descriptor_CLI_interfaces
        Then we expect the matrix
        """
        1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0
        """

    @ncomp_aminoacid_rounding
    Scenario: default parameters ncomp
        Given the file file_a containing
        """
        >OneHundredAlanines_TwoHundredCytosines
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAACCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        """
        Given file file_a as parameter
        When we run ncomposition_command_line from descriptor_CLI_interfaces
        Then we expect the matrix
        """
        0.33333 0.66667  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
        """

    @ncomp_nucleic_acids_ncomp
    Scenario: ncomp with a delimiter and DNA alphabet
        Given the file file_a containing
        """
        >test1
        ATGC
        """
        Given the file file_b containing
        """
        A,T,G,C
        """
        Given parameter -d = ,
        Given file parameter --alphabet = file_b
        Given file file_a as parameter
        When we run ncomposition_command_line from descriptor_CLI_interfaces
        Then we expect the matrix
        """
        0.25,0.25,0.25,0.25
        """
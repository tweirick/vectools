# Created by raphael at 13.05.16
Feature: #Enter feature name here
  # Enter feature description here
  @inout
  Scenario: We can handle float matrices
    Given the file test containing
    """
    1.0 2.0 3.0 4.0
    2.0 3.3 4.0 5.0
    3.0 4.0 2.0 7.0
    """
    When we read in the test file
    And print it
    Then we expect the matrix
    """
    1.0 2.0 3.0 4.0
    2.0 3.3 4.0 5.0
    3.0 4.0 2.0 7.0
    """

  @inout
  Scenario: We can handle float matrices and convert them into int if possible
    Given the file test containing
    """
    1.0 2.0 3.0 4.0
    2.0 3.0 4.0 5.0
    3.0 4.0 2.0 7.0
    """
    When we read in the test file
    And print it
    Then we expect the matrix
    """
    1 2 3 4
    2 3 4 5
    3 4 2 7
    """

  @inout
  Scenario: We can handle int matrices
    Given the file test containing
    """
    1 2 3 4
    2 3 4 5
    3 4 2 7
    """
    When we read in the test file
    And print it
    Then we expect the matrix
    """
    1 2 3 4
    2 3 4 5
    3 4 2 7
    """

  @inout
  Scenario: We can handle string matrices
    Given the file test containing
    """
    1 a 3 4
    2 c 4 5.0
    3 4 b 7
    """
    When we read in the test file
    And print it
    Then we expect the matrix
    """
    1 a 3 4
    2 c 4 5.0
    3 4 b 7
    """

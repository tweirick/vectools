# Created by raphael at 18.05.16
Feature: #Enter feature name here
  # Enter feature description here

  @z_score_normalization
  Scenario: simple z_score_normalization
    Given the file test containing
    """
    id,a,b,c,d,e,f
    0,1,2,3,4,5,6
    1,2,3,4,5,6,7
    2,3,4,5,6,7,8
    3,4,5,6,7,8,9
    4,5,6,7,8,9,10
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    Given last parameter --row-titles
    When we run z_score_normalization from normalization
    Then we expect the matrix
    """
    id,a,b,c,d,e,f
    0,-1.41421356237,-1.41421356237,-1.41421356237,-1.41421356237,-1.41421356237,-1.41421356237
    1,-0.707106781187,-0.707106781187,-0.707106781187,-0.707106781187,-0.707106781187,-0.707106781187
    2,0.0,0.0,0.0,0.0,0.0,0.0
    3,0.707106781187,0.707106781187,0.707106781187,0.707106781187,0.707106781187,0.707106781187
    4,1.41421356237,1.41421356237,1.41421356237,1.41421356237,1.41421356237,1.41421356237
    """

@z_score_normalization
  Scenario: z_score_normalization with given file for mean 0 and sd 1
    Given the file test containing
    """
    id,V1,V2,V3,V4
    1,2,4,4,5
    2,5,14,4,7
    3,4,8,6,9
    4,3,8,5,8
    5,3,9,3,5
    """
    Given file test as parameter
    Given last parameter --column-titles
    Given parameter --delimiter = ,
    Given the file meansd containing
    """
    0,0,0,0,0
    1,1,1,1,1
    """
    Given last parameter --mean-stddev
    Given file meansd as parameter
    When we run z_score_normalization from normalization
    Then we expect the matrix
    """
    id,V1,V2,V3,V4
    1.0,2.0,4.0,4.0,5.0
    2.0,5.0,14.0,4.0,7.0
    3.0,4.0,8.0,6.0,9.0
    4.0,3.0,8.0,5.0,8.0
    5.0,3.0,9.0,3.0,5.0
    """

@z_score_normalization
  Scenario: z_score_normalization with given file for mean 1 and sd 1
    Given the file test containing
    """
    id,V1,V2,V3,V4
    1,2,4,4,5
    2,5,14,4,7
    3,4,8,6,9
    4,3,8,5,8
    5,3,9,3,5
    """
    Given file test as parameter
    Given last parameter --column-titles
    Given parameter --delimiter = ,
    Given the file meansd containing
    """
    1,1,1,1,1
    1,1,1,1,1
    """
    Given last parameter --mean-stddev
    Given file meansd as parameter
    When we run z_score_normalization from normalization
    Then we expect the matrix
    """
    id,V1,V2,V3,V4
    0.0,1.0,3.0,3.0,4.0
    1.0,4.0,13.0,3.0,6.0
    2.0,3.0,7.0,5.0,8.0
    3.0,2.0,7.0,4.0,7.0
    4.0,2.0,8.0,2.0,4.0
    """
@z_score_normalization
  Scenario: z_score_normalization with given file for mean 0 and sd 2
    Given the file test containing
    """
    id,V1,V2,V3,V4
    1,2,4,4,5
    2,5,14,4,7
    3,4,8,6,9
    4,3,8,5,8
    5,3,9,3,5
    """
    Given file test as parameter
    Given last parameter --column-titles
    Given parameter --delimiter = ,
    Given the file meansd containing
    """
    0,0,0,0,0
    2,2,2,2,2
    """
    Given last parameter --mean-stddev
    Given file meansd as parameter
    When we run z_score_normalization from normalization
    Then we expect the matrix
    """
    id,V1,V2,V3,V4
    0.5,1.0,2.0,2.0,2.5
    1.0,2.5,7.0,2.0,3.5
    1.5,2.0,4.0,3.0,4.5
    2.0,1.5,4.0,2.5,4.0
    2.5,1.5,4.5,1.5,2.5
    """

  Scenario: simple quantile_normalization
    Given the file test containing
    """
    id,V1,V2,V3,V4
    1,2,4,4,5
    2,5,14,4,7
    3,4,8,6,9
    4,3,8,5,8
    5,3,9,3,5
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    Given last parameter --row-titles
    When we run quantile_normalization from normalization
    #TODO: check if this is correct
    Then we expect the matrix
    """
    id,V1,V2,V3,V4
    1,3.5,3.5,5.0,3.5
    2,8.5,8.5,5.0,5.5
    3,6.5,5.0,8.5,8.5
    4,5.0,5.0,6.5,6.5
    5,5.0,6.5,3.5,3.5
    """


  Scenario: simple median_polish_normalization
    Given the file test containing
    """
    -15 4 1
    6 16 30
    -5 4 -12
    """
    Given file test as parameter
    Given parameter --iterations = 1
    When we run median_polish_normalization from normalization
    Then we expect the matrix
    """
    -6.0 0.0 0.0
    0.0 -3.0  14.0
    10.0  6.0 -7.0
    """


  Scenario: moderate median_polish_normalization
    Given the file test containing
    """
    id,a,b,c
    0,-15,4,1
    1,6,16,30
    2,-5,4,-12
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given parameter --iterations = 2
    Given last parameter --column-titles
    Given last parameter --row-titles
    When we run median_polish_normalization from normalization
    Then we expect the matrix
    """
    id,a,b,c
    0,-6.0,0.0,0.0
    1,0.0,-3.0,14.0
    2,4.0,0.0,-13.0
    """



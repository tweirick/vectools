Feature: This section tests the behavior for the math functions in Vectools.
  # Enter feature description here

  Scenario: basic add
    Given the file a containing
    """
    1 2 3 4
    2 3 4 5
    3 4 2 7
    """
    Given the file b containing
    """
    5 4 3 2
    1 0 1 2
    3 4 3 4
    """
    Given file a as parameter
    Given file b as parameter
    When we run add from mathematics
    Then we expect the matrix
    """
    6 6 6 6
    3 3 5 7
    6 8 5 11
    """

  Scenario: basic add small
    Given the file a containing
    """
    1
    """
    Given the file b containing
    """
    5
    """
    Given file a as parameter
    Given file b as parameter
    When we run add from mathematics
    Then we expect the matrix
    """
    6
    """

  Scenario: add single matrix with scalar convenience function
    Given the file a containing
    """
    1 0 0
    0 1 0
    0 0 1
    """
    Given file a as parameter
    Given parameter --scalar = 1
    When we run add from mathematics
    Then we expect the matrix
    """
    2 1 1
    1 2 1
    1 1 2
    """


  Scenario: add float scalar to single int matrix
    Given the file a containing
    """
    1 0 0
    0 1 0
    0 0 1
    """
    Given file a as parameter
    Given parameter --scalar = 1.1
    When we run add from mathematics
    Then we expect the matrix
    """
    2.1 1.1 1.1
    1.1 2.1 1.1
    1.1 1.1 2.1
    """


  Scenario: add check if param column-titles work
    Given the file a containing
    """
    a b c d
    1 2 3 4
    2 3 4 5
    3 4 2 7
    """
    Given the file b containing
    """
    e f g h
    5 4 3 2
    1 0 1 2
    3 4 3 4
    """
    Given last parameter --column-titles
    Given file a as parameter
    Given file b as parameter
    When we run add from mathematics
    Then we expect the matrix
    """
    a b c d
    6 6 6 6
    3 3 5 7
    6 8 5 11
    """

  Scenario: add check if param row-titles work
    Given the file a containing
    """
    a 1 2 3 4
    b 2 3 4 5
    c 3 4 2 7
    """
    Given the file b containing
    """
    d 5 4 3 2
    e 1 0 1 2
    f 3 4 3 4
    """
    Given last parameter --row-titles
    Given file a as parameter
    Given file b as parameter
    When we run add from mathematics
    Then we expect the matrix
    """
    a 6 6 6 6
    b 3 3 5 7
    c 6 8 5 11
    """

  Scenario: add check if param row-titles and column-titles work
    Given the file a containing
    """
    id b c d e
    a 1 2 3 4
    b 2 3 4 5
    c 3 4 2 7
    """
    Given the file b containing
    """
    id b c d e
    d 5 4 3 2
    e 1 0 1 2
    f 3 4 3 4
    """
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given file a as parameter
    Given file b as parameter
    When we run add from mathematics
    Then we expect the matrix
    """
    id b c d e
    a 6 6 6 6
    b 3 3 5 7
    c 6 8 5 11
    """

  Scenario: add check if param row-titles, delimiter, and column-titles work
    Given the file a containing
    """
    id,b,c,d,e
    a,1,2,3,4
    b,2,3,4,5
    c,3,4,2,7
    """
    Given the file b containing
    """
    id,b,c,d,e
    d,5,4,3,2
    e,1,0,1,2
    f,3,4,3,4
    """
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given file a as parameter
    Given file b as parameter
    When we run add from mathematics
    Then we expect the matrix
    """
    id,b,c,d,e
    a,6,6,6,6
    b,3,3,5,7
    c,6,8,5,11
    """

  Scenario: calculate the determinant
    Given the file a containing
    """
    1 2 3 4
    2 3 4 5
    3 4 2 7
    4 9 0 3
    """
    Given file a as parameter
    When we run determinant from mathematics
    Then we expect the matrix
    """
    -62
    """

  Scenario: dot_product
    Given the file a containing
    """
    1
    2
    3
    4
    """
    Given file a as parameter
    Given the file b containing
    """
    4
    3
    2
    1
    """
    Given file b as parameter
    When we run dot_product from mathematics
    Then we expect the matrix
    """
    20
    """
  Scenario: inverse
    Given the file a containing
    """
    2 5
    1 3
    """
    Given file a as parameter
    When we run inverse from mathematics
    Then we expect the matrix
    """
    3.0 -5.0
    -1.0  2.0
    """

  Scenario: multiply matrices
    Given the file bla containing
    """
    1 2 3
    """
    Given the file blub containing
    """
    1 2
    2 3
    6 7
    """
    Given file bla as parameter
    Given file blub as parameter
    When we run multiply from mathematics
    Then we expect the matrix
    """
    23  29
    """

  Scenario: multiply matrices - 1x3 * 3x1
    Given the file a containing
    """
    1 2 3
    """
    Given the file b containing
    """
    7
    8
    9
    """
    Given file a as parameter
    Given file b as parameter
    When we run multiply from mathematics
    Then we expect the matrix
    """
    50
    """

  Scenario: multiply matrices - 3x1 * 1x3
    Given the file a containing
    """
    1 2 3
    """
    Given the file b containing
    """
    7
    8
    9
    """
    Given file b as parameter
    Given file a as parameter
    When we run multiply from mathematics
    Then we expect the matrix
    """
    7 14 21
    8 16 24
    9 18 27
    """

  Scenario: multiply matrices
    Given the file bla containing
    """
    1 2 3
    """
    Given the file blub containing
    """
    1 2
    2 3
    6 7
    """
    Given file bla as parameter
    Given file blub as parameter
    When we run multiply from mathematics
    Then we expect the matrix
    """
    23  29
    """




  Scenario: subtract
    Given the file bla containing
    """
    1 2 3
    4 5 6
    7 8 9
    """
    Given the file blub containing
    """
    9 8 7
    6 5 4
    3 2 1
    """
    Given file bla as parameter
    Given file blub as parameter
    When we run subtract from mathematics
    Then we expect the matrix
    """
    -8  -6 -4
    -2  0 2
    4 6 8
    """

  Scenario: eigen_values
    Given the file bla containing
    """
    2 0 0
    0 3 4
    0 4 9
    """
    Given file bla as parameter
    When we run eigen_values from mathematics
    Then we expect the matrix
    """
    11.0  1.0 2.0
    """


  ########################
  #  eigen_vectors
  ########################
  Scenario: eigen_vectors
    Given the file bla containing
    """
    1 0 0
    0 2 0
    0 0 3
    """
    Given file bla as parameter
    When we run eigen_vectors from mathematics
    Then we expect the matrix
    """
    1.0 0.0 0.0
    0.0 1.0 0.0
    0.0 0.0 1.0
    """

  ########################
  #  Sum
  ########################
  Scenario: sum - A simple single file containing positive integers.
    Given the file a containing
    """
    1 1 1
    0 1 1
    0 0 1
    """
    Given file a as parameter
    When we run sum_up from mathematics
    Then we expect the matrix
    """
    1 2 3
    """

  Scenario: sum - A simple single file containing positive integers and column IDs.
    Given the file a containing
    """
    C1 C2 C3
    1 1 1
    0 1 1
    0 0 1
    """
    Given last parameter --column-titles
    Given file a as parameter
    When we run sum_up from mathematics
    Then we expect the matrix
    """
    C1 C2 C3
    1 2 3
    """

  Scenario: sum - A simple single file containing positive integers and row IDs.
    Given the file a containing
    """
    R1 1 1 1
    R2 0 1 1
    R3 0 0 1
    """
    Given last parameter --row-titles
    Given file a as parameter
    When we run sum_up from mathematics
    Then we expect the matrix
    """
    1 2 3
    """


  Scenario: sum - Two simple files containing positive integers.
    Given the file a containing
    """
    1 1 1
    0 1 1
    0 0 1
    """
    Given the file b containing
    """
    1 0 0
    0 1 0
    0 0 1
    """
    Given file a as parameter
    Given file b as parameter
    When we run sum_up from mathematics
    Then we expect the matrix
    """
    2 3 4
    """


  Scenario: sum - Two files containing one with integers and one with positive floats.
    Given the file a containing
    """
    1 1 1
    0 1 1
    0 0 1
    """
    Given the file b containing
    """
    1.1 0.0 0.0
    0.0 1.0 0.0
    0.0 0.0 1.0
    """
    Given file a as parameter
    Given file b as parameter
    When we run sum_up from mathematics
    Then we expect the matrix
    """
    2.1 3.0 4.0
    """
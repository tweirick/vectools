# Created by raphael at 04.07.16
Feature: only apply on column feature
  this parameter allows a user to specify on which columns the function should be applied on. This is a shortcut to skip a slicing step

  @apply
  Scenario: add feature to max in analysis
    Given the file testfile containing
    """
    id,a,b,c,string_column
    one,1,2,3,"four"
    two,2,1,33,"four"
    three,3,0,333,"four"
    four,4,-1,3333,"four"
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2
    When we run maximum from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c
    4,2,3333
    """

  @apply
  Scenario: add feature to max in analysis with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:5
    When we run maximum from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    4,2,3333,151,667
    """

  @apply
  Scenario: add feature to mean in analysis with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:-1
    When we run average from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    2.5,0.5,925.5,55.5,183.25
    """

  @apply
  Scenario: add feature to median in analysis with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:5
    When we run median from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    2.5,0.5,183.0,33.0,36.0
    """


  @apply
  Scenario: add feature to min in analysis with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:5
    When we run minimum from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    1,-1,3,5,-6
    """

  @apply
  Scenario: add feature to percentile in analysis with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter -p = 0.5
    Given parameter --only-apply-on = 0:2,4:5
    When we run percentile from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    2.5,0.5,183.0,33.0,36.0
    """

  @apply
  Scenario: add feature to sd in analysis with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:5
    When we run sd from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    1.29099444874,1.29099444874,1611.90105155,66.6608330781,324.034334601
    """

  @apply
  Scenario: add feature to add in math with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:5
    When we run add from mathematics
    Then we expect the matrix
    """
    id,a,b,c,string_column,d,e
    one,2,4,6,four,10,12
    two,4,2,66,four,30,132
    three,6,0,666,four,102,1334
    four,8,-2,6666,four,302,-12
    """


  @apply
  Scenario: add feature to add in math with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --scalar = 5
    Given parameter --only-apply-on = 0:2,4:5
    Given file testfile as parameter
    When we run add from mathematics
    Then we expect the matrix
    """
    id,a,b,c,string_column,d,e
    one,6,7,8,four,10,11
    two,7,6,38,four,20,71
    three,8,5,338,four,56,672
    four,9,4,3338,four,156,-1
    """


  @apply
  Scenario: add feature to subtract in math with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:5
    When we run subtract from mathematics
    Then we expect the matrix
    """
    id,a,b,c,string_column,d,e
    one,0,0,0,four,0,0
    two,0,0,0,four,0,0
    three,0,0,0,four,0,0
    four,0,0,0,four,0,0
    """


  @apply
  Scenario: add feature to median_polish_normalization in normalization with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:5
    When we run median_polish_normalization from normalization
    Then we expect the matrix
    """
    id,a,b,c,d,e
    one,7.75,11.75,-152.125,-0.125,-25.125
    two,-1.0,1.0,-131.875,0.125,25.125
    three,-36.25,-36.25,131.875,-0.125,589.875
    four,1.0,-1.0,3168.125,136.125,-46.875
    """


  @apply
  Scenario: add feature to quantile_normalization in normalization with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:5
    When we run quantile_normalization from normalization
    Then we expect the matrix
    """
    id,a,b,c,d,e
    one,0.4,831.4,0.4,0.4,11.2
    two,11.2,90.8,11.2,11.2,90.8
    three,90.8,11.2,90.8,90.8,831.4
    four,831.4,0.4,831.4,831.4,0.4
    """


  @apply
  Scenario: add feature to z_score_normalization in normalization with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,a,b,c,string_column,d,e
    one,1,2,3,"four",5,6
    two,2,1,33,"four",15,66
    three,3,0,333,"four",51,667
    four,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:2,4:5
    When we run z_score_normalization from normalization
    Then we expect the matrix
    """
    id,a,b,c,d,e
    one,-1.3416407865,1.3416407865,-0.660841585548,-0.874762202864,-0.631632665347
    two,-0.4472135955,0.4472135955,-0.639350802278,-0.701541964673,-0.417821890053
    three,0.4472135955,-0.4472135955,-0.424442969579,-0.0779491071859,1.72384937581
    four,1.3416407865,-1.3416407865,1.72463535741,1.65425327472,-0.674394820406
    """


  @apply
  Scenario: add feature to pearson in analysis with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,class,a,b,c,string_column,d,e
    one,male,1,2,3,"four",5,6
    two,female,2,4,6,"four",10,12
    three,others,3,0,333,"four",51,667
    four,male,4,-1,3333,"four",151,-6
    """
    Given parameter --groups = male,female
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    Given parameter --number-of-values-above-zero = 1
    Given parameter --only-apply-on = 0:4,6:
    Given file testfile as parameter
    When we run pearson_group from analysis with tmpfile
    Then we expect the matrix
    """
    group.1_group.2,id.1_id.2,pearson_coefficient,p-value,a.1,a.2,b.1,b.2,c.1,c.2,d.1,d.2,e.1,e.2
    male_female,one_two,1.0,0.0,1.0,2.0,2.0,4.0,3.0,6.0,5.0,10.0,6.0,12.0
    """


  @apply
  Scenario: add feature to spearman in analysis with more complex string for only-apply-on
    Given the file testfile containing
    """
    id,class,a,b,c,string_column,d,e
    one,male,1,2,3,"four",5,6
    two,female,2,4,6,"four",10,12
    three,others,3,0,333,"four",51,667
    four,male,4,-1,3333,"four",151,-6
    """
    Given file testfile as parameter
    Given parameter --groups = male,female
    Given parameter --delimiter = ,
    Given parameter --number-of-values-above-zero = 1
    Given last parameter --column-titles
    Given parameter --only-apply-on = 0:4,6:
    When we run spearman from analysis with tmpfile
    Then we expect the matrix
    """
    group.1_group.2,id.1_id.2,spearmans_rho,p-value,a.1,a.2,b.1,b.2,c.1,c.2,d.1,d.2,e.1,e.2
    male_female,one_two,1.0,1.40426542205e-24,1.0,2.0,2.0,4.0,3.0,6.0,5.0,10.0,6.0,12.0
    """
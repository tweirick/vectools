Feature: vector_sort
  This feature describes the vector_sort function

  @vector_sort
  Scenario: sort a matrix with sort
    Given a matrix as STDIN
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter -k = 0
    Given the placeholder -
    When we run vector_sort from manipulation with STDIN
    Then we expect the matrix
    """
    0 1 0 1 0
    0 2 0 1 0
    1 0 1 0 1
    2 0 1 0 1
    2 0 2 0 1
    """

  @vector_sort
  Scenario: sort a matrix with vector_sort with two columns
    Given a matrix as STDIN
    """
    1 0 1 0 1
    0 2 0 1 0
    2 0 1 0 1
    0 1 0 1 0
    2 0 2 0 1
    """
    Given parameter --keys = 0,1
    Given the placeholder -
    When we run vector_sort from manipulation with STDIN
    Then we expect the matrix
    """
    0 1 0 1 0
    0 2 0 1 0
    1 0 1 0 1
    2 0 1 0 1
    2 0 2 0 1
    """

  @vector_sort
  Scenario: sort a matrix with vector_sort with two parameter - check priority
    Given a matrix as STDIN
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter -k = 1,0
    Given the placeholder -
    When we run vector_sort from manipulation with STDIN
    Then we expect the matrix
    """
    1 0 1 0 1
    2 0 1 0 1
    2 0 2 0 1
    0 1 0 1 0
    0 2 0 1 0
    """

  @vector_sort
  Scenario: check params for vector_sort for both row-titles and column-titles
    Given the file matrix1 containing
    """
    id,c,d,e
    a,1,2,3
    b,3,4,5
    c,1,1,0
    """
    Given parameter -k = 0,2
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    Given file matrix1 as parameter
    When we run vector_sort from manipulation with tmpfile
    Then we expect the matrix
    """
    id,c,d,e
    c,1,1,0
    a,1,2,3
    b,3,4,5
    """

  @vector_sort
  Scenario: check params for vector_sort for column-titles
    Given the file test containing
    """
    c,d,e
    1,2,3
    3,4,5
    1,1,0
    """
    Given parameter -k = 0,2
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    Given file test as parameter
    When we run vector_sort from manipulation with tmpfile
    Then we expect the matrix
    """
    c,d,e
    1,1,0
    1,2,3
    3,4,5
    """

  @vector_sort
  Scenario: check params for vector_sort for row-titles
    Given the file test containing
    """
    a,1,2,3
    b,3,4,5
    c,1,1,0
    """
    Given parameter -k = 0,2
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given file test as parameter
    When we run vector_sort from manipulation with tmpfile
    Then we expect the matrix
    """
    c,1,1,0
    a,1,2,3
    b,3,4,5
    """

  @vector_sort
  Scenario: sort a matrix with vector_sort with two parameter reversed
    Given the file test containing
    """
    1,0,1,0,1
    0,1,0,1,0
    2,0,1,0,1
    0,2,0,1,0
    2,0,2,0,1
    """
    Given parameter -k = 1,0
    Given parameter --delimiter = ,
    Given last parameter --descending
    Given file test as parameter
    When we run vector_sort from manipulation with tmpfile
    Then we expect the matrix
    """
    0,2,0,1,0
    0,1,0,1,0
    2,0,1,0,1
    2,0,2,0,1
    1,0,1,0,1
    """
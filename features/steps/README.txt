#Behave README
##Behave command line and settings
This is the help file for writing integration test with behave in vectortools:

To start behave you have to type "behave" without any argument.
Due to playing with tmp files, argv and other command line affecting things it is not possible to set options to behave via parameters.
To change the behavior of behave, change the .behaverc in the root directory:

For a full list of settings go to https://pythonhosted.org/behave/behave.html. Scroll down to recognised settings.
Useful settings are:

stop=true           # Stops after first failing test

tags=-@pending      # will execute all tests except those tagged by @pending

tags=@join          # will execute only tests tagged by @join
     @apply         # and @apply. The others will be on command line as skipped tests


show_skipped=no     # cleans your command window up. Skipped tests will not be shown

##Writing tests
A Feature is a bundle of tests describing a (new) feature, improvement,
or new functionality like adding a parameter to all functions.

To add a feature, create a file features/<feature_to_test_for>.feature.

`
Feature: #Enter feature name here
  # Enter feature description here

  Scenario: # Enter scenario name here
    # Enter steps here
`

Is the basic scaffold of an integration test. An integration test is (almost) written in plain text.
In general a step begins with the keywords given, when, then:

* 'Given' tells you what is the background of the test. In vectortools, most of the time we use matrices.
   So 'given' is a matrix. Also parameters belong to given steps.

* 'When' tells you, what happened with the data or what function you will use.
  "When we apply sum to the matrix" or something like this

*'Then' tells you what you expect happened after calling your functions in the when block.

How knows behave what to do when you write everything in (nearly) plain text? The magic happens in the step definition!

In features/step/ you can create a file with step definitions.

In vectortools there are some steps defined, which might help you easily test your features:

@given("parameter {} = {}")

here you can set a parameter with:
"given parameter --foo = bar"
which adds to the args "--foo" and "bar"

@given("the file {} containing")

With this step you can create tmp files with the content defined by a multilinestring after this step.
Remember: whitespaces will be converted into tabs.
'given the file testfile containing
"""
1,2,3,4
2,3,4,5
6,5,4,3
"""'
After this you have to use the step
@given("file {} as parameter")
'given file testfile as parameter'
to append the file name to argv.

@then("we expect the matrix")

Same as 'given the file {} containing' but for the matrix you expect as the result

'then we expect the matrix
"""
9 8 7 4
1 2 3 5
"""
'

@given("last parameter {}")

A given step to set a last parameter. If you call it twice the last one is the last one.
It just appends the parameter to argv.

If you want to add a test for a new file in lib you have to define a step like this:
from behave import *
from lib import mathematics

use_step_matcher("parse")

@when("we run {} from mathematics")
def step_impl(context, function):
    """
    :type context: behave.runner.Context
    """
    result = getattr(mathematics, function)(context.parser)

where you have to change mathematics to the name of your lib file.
After this you can call functions with:

"when we run add from mathematics"
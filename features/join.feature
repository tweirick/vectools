# Created by raphael at 22.06.16
Feature: Join
  feature test for the join function and every subfunction:
  LEFT,INNER,UNION,OUTER


  @join
  Scenario: default parameters join
    Given the file file_a containing
    """
    a 1 3
    b 2 5
    c 3 4
    """
    Given the file file_b containing
    """
    a 1
    b 2
    c 3
    """
    Given parameter -k1 = 0
    Given parameter -k2 = 0
    Given file file_a as parameter
    Given file file_b as parameter
    When we run join from manipulation with tmpfile
    Then we expect the matrix
    """
    a 1 3 a 1
    b 2 5 b 2
    c 3 4 c 3
    """


  @join
  Scenario: default parameters join, other keys
    Given the file file_a containing
    """
    a 1 3
    b 2 5
    c 3 4
    """
    Given the file file_b containing
    """
    1 a 1
    2 b 2
    3 c 3
    """
    Given parameter -k1 = 0
    Given parameter -k2 = 1
    Given file file_a as parameter
    Given file file_b as parameter
    When we run join from manipulation with tmpfile
    Then we expect the matrix
    """
    a 1 3 1 a 1
    b 2 5 2 b 2
    c 3 4 3 c 3
    """

  @join
  Scenario: default parameters join with delimiter
    Given the file file_a containing
    """
    a,1,3
    b,2,5
    c,3,4
    """
    Given the file file_b containing
    """
    a,1
    b,2
    c,3
    """
    Given parameter -k1 = 0
    Given parameter -k2 = 0
    Given parameter --delimiter = ,
    Given file file_a as parameter
    Given file file_b as parameter
    When we run join from manipulation with tmpfile
    Then we expect the matrix
    """
    a,1,3,a,1
    b,2,5,b,2
    c,3,4,c,3
    """

  @join
  Scenario: slicing for the second file
    Given the file file_a containing
    """
    a,1,3
    b,2,5
    c,3,4
    """
    Given the file file_b containing
    """
    a,1
    b,2
    c,3
    """
    Given parameter -k1 = 0
    Given parameter -k2 = 0
    Given parameter --delimiter = ,
    Given parameter --slice = 1
    Given file file_a as parameter
    Given file file_b as parameter
    When we run join from manipulation with tmpfile
    Then we expect the matrix
    """
    a,1,3,1
    b,2,5,2
    c,3,4,3
    """

  @join
  Scenario: what about slice with colon
    Given the file file_a containing
    """
    a,1,3
    b,2,5
    c,3,4
    """
    Given the file file_b containing
    """
    a,1,3,1,1,2,3
    b,2,5,2,1,2,3
    c,3,4,3,1,2,3
    """
    Given parameter -k1 = 0
    Given parameter -k2 = 0
    Given parameter --slice = 1:4
    Given parameter --delimiter = ,
    Given file file_a as parameter
    Given file file_b as parameter
    When we run join from manipulation with tmpfile
    Then we expect the matrix
    """
    a,1,3,1,3,1,1
    b,2,5,2,5,2,1
    c,3,4,3,4,3,1
    """

  @join
  Scenario: what about six files
    Given the file file_a containing
    """
    a,1,3
    b,2,5
    c,3,4
    """
    Given the file file_b containing
    """
    a,1,3,1,1,2,3
    b,2,5,2,1,2,3
    c,3,4,3,1,2,3
    """
    Given parameter -k1 = 0
    Given parameter -k2 = 0
    Given parameter --delimiter = ,
    Given file file_a as parameter
    Given file file_b as parameter
    Given file file_b as parameter
    Given file file_b as parameter
    Given file file_b as parameter
    Given file file_b as parameter
    When we run join from manipulation with tmpfile
    Then we expect the matrix
    """
    a,1,3,a,1,3,1,1,2,3,a,1,3,1,1,2,3,a,1,3,1,1,2,3,a,1,3,1,1,2,3,a,1,3,1,1,2,3
    b,2,5,b,2,5,2,1,2,3,b,2,5,2,1,2,3,b,2,5,2,1,2,3,b,2,5,2,1,2,3,b,2,5,2,1,2,3
    c,3,4,c,3,4,3,1,2,3,c,3,4,3,1,2,3,c,3,4,3,1,2,3,c,3,4,3,1,2,3,c,3,4,3,1,2,3
    """


  @join
  Scenario: what about six files and slicing?
    Given the file file_a containing
    """
    a,1,3
    b,2,5
    c,3,4
    """
    Given the file file_b containing
    """
    a,1,3,1,1,2,3
    b,2,5,2,1,2,3
    c,3,4,3,1,2,3
    """
    Given parameter -k1 = 0
    Given parameter -k2 = 0
    Given parameter --delimiter = ,
    Given parameter --slice = 1:4
    Given file file_a as parameter
    Given file file_b as parameter
    Given file file_b as parameter
    Given file file_b as parameter
    Given file file_b as parameter
    Given file file_b as parameter
    When we run join from manipulation with tmpfile
    Then we expect the matrix
    """
    a,1,3,1,3,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,3,1,1
    b,2,5,2,5,2,1,2,5,2,1,2,5,2,1,2,5,2,1,2,5,2,1
    c,3,4,3,4,3,1,3,4,3,1,3,4,3,1,3,4,3,1,3,4,3,1
    """
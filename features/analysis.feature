Feature:  Tests for the analysis part of Vectools

  # ==========================================================================
  #  confmat
  # ==========================================================================

  Scenario: Test the operation for making confusion matrices.
    Given we use vectools confmat
    Given argument --delimiter with the value ,
    Given the file multi_label.tsv containing the following text
    """
    2,0
    0,0
    2,2
    2,2
    0,0
    1,2
    """
    When we run the command
    Then we expect the output
    """
    _,0,1,2
    0,2,0,0
    1,0,0,1
    2,1,0,2
    """

  Scenario: Test the operation for making confusion matrices delimited with spaces.
    Given we use vectools confmat
    Given argument --delimiter with the value " "
    Given the argument -c
    Given the file multi_label.col_names.tsv containing the following text
    """
    T P
    2 0
    0 0
    2 2
    2 2
    0 0
    1 2
    """
    When we run the command
    Then we expect the output
    """
    _ 0 1 2
    0 2 0 0
    1 0 0 1
    2 1 0 2
    """

  Scenario: Test the operation for making confusion matrices delimited with tabs.
    Given we use vectools confmat
    Given the argument -c
    Given the file multi_label.col_names.tsv containing the following text
    """
    T	P
    2	0
    0	0
    2	2
    2	2
    0	0
    1	2
    """
    When we run the command
    Then we expect the output
    """
    _	0	1	2
    0	2	0	0
    1	0	0	1
    2	1	0	2
    """

  Scenario: Test the operation for making confusion matrices delimited with spaces with row and column titles.
    Given we use vectools confmat
    Given argument --delimiter with the value " "
    Given the argument -r
    Given the argument -c
    Given the file multi_label.col_names.tsv containing the following text
    """
     T P
    1 2 0
    2 0 0
    3 2 2
    4 2 2
    5 0 0
    6 1 2
    """
    When we run the command
    Then we expect the output
    """
    _ 0 1 2
    0 2 0 0
    1 0 0 1
    2 1 0 2
    """


  Scenario: Test the operation for making confusion matrices matrix passed via STDIN.
    Given the following text passed via STDIN
    """
    TC,PC
    cat,ant
    ant,ant
    cat,cat
    cat,cat
    ant,ant
    bird,cat
    """
    Given we use vectools confmat
    Given argument --delimiter with the value ","
    Given the argument -c
    Given the argument -
    When we run the command
    Then we expect the output
    """
    _,ant,bird,cat
    ant,2,0,0
    bird,0,0,1
    cat,1,0,2
    """


  # ==========================================================================
  #  max
  # ==========================================================================

  Scenario: Test the max operation.
    Given we use vectools max
    Given argument --delimiter with the value ,
    Given the file matrix.csv containing the following text
    """
    5,50,12500,98,1
    8,13,3250,28,1
    3,16,4000,35,1
    2,20,5000,45,1
    1,24,6000,77,-1
    """
    When we run the command
    Then we expect the output
    """
    8,50,12500,98,1
    """


  Scenario: run a simple test for max
    Given a 4x4 matrix with zeros
    When we run maximum from analysis
    Then there should be 4 zeros on the console


  Scenario: run params test for max both r and c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    When we run maximum from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    4,5,6,7,8
    """

  Scenario: run params test for max c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    When we run maximum from analysis with tmpfile
    Then we expect the matrix
    """
    id,a,b,c,d,e
    3,4,5,6,7,8
    """

  Scenario: run params test for max r
    Given the file test containing
    """
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    When we run maximum from analysis with tmpfile
    Then we expect the matrix
    """
    4,5,6,7,8
    """


  Scenario: run another test for max
    Given a int matrix
      """
      1 2 3 4 5
      2 1 5 3 4
      3 4 2 5 1
      4 5 1 2 3
      5 3 4 1 2
      """
    When we run maximum from analysis
    Then there should be 5 5 times on the console

  Scenario: run a simple test for min
    Given a 4x4 matrix with zeros
    When we run minimum from analysis
    Then there should be 4 zeros on the console

  Scenario: run another test for min
    Given a int matrix
      """
      1 2 3 4 5
      2 1 5 3 4
      3 4 2 5 1
      4 5 1 2 3
      5 3 4 1 2
      """
    When we run minimum from analysis
    Then there should be 1 5 times on the console

  Scenario: run params test for min both r and c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    When we run minimum from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    1,2,3,4,5
    """

  Scenario: run params test for min c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    When we run minimum from analysis with tmpfile
    Then we expect the matrix
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    """

  Scenario: run params test for max r
    Given the file test containing
    """
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    When we run minimum from analysis with tmpfile
    Then we expect the matrix
    """
    1,2,3,4,5
    """


  #Scenario: run a simple test for sd
  #  Given a 4x4 matrix with zeros
  #  When we run sd from analysis
  #  Then there should be 4 zeros on the console

  Scenario: run params test for sd both r and c
    Given the file test containing
    """
    id,a,b,c,d,e
    r1,5,1,9,0,5
    r2,2,1,4,0,6
    r3,3,1,5,0,7
    r4,8,5,9,7,8
    """
    Given parameter --delimiter = ,
    Given file test as parameter
    Given last parameter --column-titles
    Given last parameter --row-titles
    When we run sd from analysis with tmpfile
    Then we expect the matrix
    """
    id,a,b,c,d,e,standard_deviation
    r1,5.0,1.0,9.0,0.0,5.0,3.2249
    r2,2.0,1.0,4.0,0.0,6.0,2.15407
    r3,3.0,1.0,5.0,0.0,7.0,2.56125
    r4,8.0,5.0,9.0,7.0,8.0,1.35647
    """

  Scenario: run params test for sd c
    Given the file test containing
    """
    a,b,c,d,e,f
    0,1,2,3,4,5
    1,2,3,2,5,2
    7,3,4,7,6,7
    9,1,5,6,7,9
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    When we run sd from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e,f,standard_deviation
    0.0,1.0,2.0,3.0,4.0,5.0,1.70783
    1.0,2.0,3.0,2.0,5.0,2.0,1.25831
    7.0,3.0,4.0,7.0,6.0,7.0,1.59861
    9.0,1.0,5.0,6.0,7.0,9.0,2.73354
    """

  Scenario: run params test for sd r
    Given the file test containing
    """
    R1,5,1,9,0,5
    R2,2,1,4,0,6
    R3,3,1,5,0,7
    R4,8,5,9,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    When we run sd from analysis with tmpfile
    Then we expect the matrix
    """
    R1,5.0,1.0,9.0,0.0,5.0,3.2249
    R2,2.0,1.0,4.0,0.0,6.0,2.15407
    R3,3.0,1.0,5.0,0.0,7.0,2.56125
    R4,8.0,5.0,9.0,7.0,8.0,1.35647
    """


  #Scenario: run another test for standard deviation
  #  Given a int matrix
  #    """
  #    1 2 3 4 5
  #    2 1 5 3 4
  #    3 4 2 5 1
  #    4 5 1 2 3
  #    5 3 4 1 2
  #    """
  #  When we run sd from analysis
  #  Then there should be 1.58113 5 times on the console


  Scenario: run a simple test for average
    Given we use vectools mean
    Given argument --delimiter with the value ,
    Given argument --roundto with the value 0
    Given the file zeros.tsv containing the following text
    """
    0,0,0,0
    0,0,0,0
    0,0,0,0
    0,0,0,0
    """
    When we run the command
    Then we expect the output
    """
    0,0,0,0
    """

  Scenario: run params test for average both r and c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    When we run mean from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    2.5,3.5,4.5,5.5,6.5
    """

  Scenario: run params test for average c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    When we run mean from analysis with tmpfile
    Then we expect the matrix
    """
    id,a,b,c,d,e
    1.5,2.5,3.5,4.5,5.5,6.5
    """

  Scenario: run params test for max r
    Given the file test containing
    """
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    When we run mean from analysis with tmpfile
    Then we expect the matrix
    """
    2.5,3.5,4.5,5.5,6.5
    """


  #Scenario: run another test for average
  #  Given the file test containing
  #    """
  #    1 2 3 4 5
  #    2 1 5 3 4
  #    3 4 2 5 1
  #    4 5 1 2 3
  #    5 3 4 1 2
  #    """
  #  Given file test as parameter
  #  When we run mean from analysis
  #  Then there should be 3 5 times on the console

  Scenario: run a simple test for median
    Given a 4x4 matrix with zeros
    When we run median from analysis
    Then there should be 4 zeros on the console


  Scenario: run params test for median both r and c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    When we run median from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    2.5,3.5,4.5,5.5,6.5
    """

  Scenario: run params test for median c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    When we run median from analysis with tmpfile
    Then we expect the matrix
    """
    id,a,b,c,d,e
    1.5,2.5,3.5,4.5,5.5,6.5
    """

  Scenario: run params test for median r
    Given the file test containing
    """
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    When we run median from analysis with tmpfile
    Then we expect the matrix
    """
    2.5,3.5,4.5,5.5,6.5
    """

  Scenario: run another test for median
    Given a int matrix
      """
      1 2 3 4 5
      2 1 5 3 4
      3 4 2 5 1
      4 5 1 2 3
      5 3 4 1 2
      """
    When we run median from analysis
    Then there should be 3 5 times on the console


  #Scenario: run a simple test for pca
  #  Given a 4x4 diag matrix with 4
  #  When we run run_pca from analysis
  #  Then there should be comp 4 times on the console
  #  Then there should be 0.5 4 times on the console
  #  Then there should be 0.288 3 times on the console



  Scenario: run a simple test for percentile
    Given a 4x4 matrix with zeros
    Given parameter --percentile = 0.5
    When we run percentile from analysis
    Then there should be 4 zeros on the console

  Scenario: run params test for percentile both r and c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --percentile = 0.5
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    When we run percentile from analysis with tmpfile
    Then we expect the matrix
    """
    a,b,c,d,e
    2.5,3.5,4.5,5.5,6.5
    """

  Scenario: run params test for percentile c
    Given the file test containing
    """
    id,a,b,c,d,e
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --percentile = 0.5
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    When we run percentile from analysis with tmpfile
    Then we expect the matrix
    """
    id,a,b,c,d,e
    1.5,2.5,3.5,4.5,5.5,6.5
    """

  Scenario: run params test for percentile r
    Given the file test containing
    """
    0,1,2,3,4,5
    1,2,3,4,5,6
    2,3,4,5,6,7
    3,4,5,6,7,8
    """
    Given file test as parameter
    Given parameter --percentile = 0.5
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    When we run percentile from analysis with tmpfile
    Then we expect the matrix
    """
    2.5,3.5,4.5,5.5,6.5
    """


  Scenario: run a simple test for pearson
    Given a matrix
      """
      a male 1 2 3 4
      b female 2 4 6 8
      """
    Given parameter --groups = male,female
    Given parameter --number-of-values-above-zero = 1
    When we run pearson_group from analysis
    Then there should be a 1.0 on column 3
    Then there should be male_female on the console
    Then there should be a_b on the console

  Scenario: run a params test for pearson
    Given the file test containing
      """
      id,gender,val1,val2,val3,val4
      a,male,1,2,3,4
      b,female,2,4,6,8
      """
    Given file test as parameter
    Given parameter --groups = male,female
    Given parameter --number-of-values-above-zero = 1
    Given last parameter --column-titles
    Given parameter --delimiter = ,
    When we run pearson_group from analysis with tmpfile
    Then we expect the matrix
    """
    group.1_group.2,id.1_id.2,pearson_coefficient,p-value,val1.1,val1.2,val2.1,val2.2,val3.1,val3.2,val4.1,val4.2
    male_female,a_b,1.0,0.0,1.0,2.0,2.0,4.0,3.0,6.0,4.0,8.0
    """

  Scenario: run a simple test for Spearman
    Given a matrix
      """
      a male 1 2 3 4
      b female 2 4 6 8
      """
    Given parameter --groups = male,female
    Given parameter --number-of-values-above-zero = 1
    When we run spearman from analysis
    Then there should be a 1.0 on column 3
    Then there should be male_female on the console
    Then there should be a_b on the console

  Scenario: run a params test for Spearman
    Given the file test containing
      """
      id,gender,val1,val2,val3,val4
      a,male,1,2,3,4
      b,female,2,4,6,8
      """
    Given file test as parameter
    Given parameter --groups = male,female
    Given parameter --number-of-values-above-zero = 1
    Given last parameter --column-titles
    Given parameter --delimiter = ,
    When we run spearman from analysis with tmpfile
    Then we expect the matrix
    """
    group.1_group.2,id.1_id.2,spearmans_rho,p-value,val1.1,val1.2,val2.1,val2.2,val3.1,val3.2,val4.1,val4.2
    male_female,a_b,1.0,0.0,1.0,2.0,2.0,4.0,3.0,6.0,4.0,8.0
    """

  Scenario: run a more specific test for Spearman
    Given a matrix
      """
      course1 English 	56 	75 	45 	71 	62 	64 	58 	80 	76 	61
      course2 Maths 	66 	70 	40 	60 	65 	56 	59 	77 	67 	63
      """
    Given parameter --groups = English,Maths
    Given parameter --number-of-values-above-zero = 1
    Given parameter --spearmans-rank-correlation-threshold = 0.66
    Given parameter --p-value-threshold = 0.1
    When we run spearman from analysis
    Then there should be a spearmans_rho on column 3
    Then there should be a 0.67 on column 3
    Then there should be a 0.033 on column 4
    Then there should be English_Maths on the console
    Then there should be course1_course2 on the console
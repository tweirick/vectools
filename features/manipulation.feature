Feature: Tests for the manipulation part of Vectools
  # Enter feature description here

  Scenario: check params for transpose
    Given the file test containing
    """
    id,a,b
    c,1,2
    d,3,4
    """
    Given parameter --delimiter = ,
    Given file test as parameter
    When we run transpose from manipulation with tmpfile
    Then we expect the matrix
    """
    id,c,d
    a,1,3
    b,2,4
    """

  Scenario: A transpose of a 1x2 matrix of strings.
    Given a matrix as STDIN
      """
      ab
      cd
      """
    Given the placeholder -
    When we run transpose from manipulation with STDIN
    Then we expect the matrix
      """
      ab cd
      """

  Scenario: A transpose of a 1x2 matrix of integers.
    Given a matrix as STDIN
      """
      4
      5
      """
    Given the placeholder -
    When we run transpose from manipulation with STDIN
    Then we expect the matrix
      """
      4 5
      """

  Scenario: run a simple test for transpose
    Given a matrix as STDIN
      """
      1 0 0 0 0
      0 1 0 0 0
      0 0 1 0 0
      0 0 0 1 0
      0 0 0 0 1
      """
    Given the placeholder -
    When we run transpose from manipulation with STDIN
    Then we expect the matrix
      """
      1 0 0 0 0
      0 1 0 0 0
      0 0 1 0 0
      0 0 0 1 0
      0 0 0 0 1
      """

  Scenario: run another test for transpose
    Given a matrix as STDIN
      """
      1 0 1 0 1
      0 1 0 1 0
      2 0 1 0 1
      0 2 0 1 0
      2 0 2 0 1
      """
    Given the placeholder -
    When we run transpose from manipulation with STDIN
    Then we expect the matrix
      """
      1 0 2 0 2
      0 1 0 2 0
      1 0 1 0 2
      0 1 0 1 0
      1 0 1 0 1
      """

  Scenario: basic vrep
    Given the file a containing
      """
      a b c
      x y z
      """
    Given the file b containing
      """
      a b c
      d e f
      c b a
      """
    Given file a as parameter
    Given file b as parameter
    When we run vrep from manipulation with tmpfile
    Then we expect the matrix
      """
      a b c
      """

  Scenario: run a test for concatenate with axis 1
    Given the file matrix1 containing
      """
      1 0 1 0 1
      0 1 0 1 0
      2 0 1 0 1
      0 2 0 1 0
      2 0 2 0 1
      """
    Given the file matrix2 containing
      """
      1 0 1 0 1
      0 1 0 1 0
      2 0 1 0 1
      0 2 0 1 0
      2 0 2 0 1
      """
    Given parameter --axis = 1
    Given file matrix1 as parameter
    Given file matrix2 as parameter
    When we run concatenate from manipulation with tmpfile
    Then we expect the matrix
      """
      1 0 1 0 1 1 0 1 0 1
      0 1 0 1 0 0 1 0 1 0
      2 0 1 0 1 2 0 1 0 1
      0 2 0 1 0 0 2 0 1 0
      2 0 2 0 1 2 0 2 0 1
      """


  Scenario: check params for concatenate with axis 1
    Given the file test containing
    """
    id,c,d
    a,1,2
    b,3,4
    """
    Given file test as parameter
    Given file test as parameter
    Given parameter --axis = 1
    Given parameter --delimiter = ,
    When we run concatenate from manipulation with tmpfile
    Then we expect the matrix
    """
    id,c,d,id,c,d
    a,1,2,a,1,2
    b,3,4,b,3,4
    """

  Scenario: run a test for concatenate with standard behavior (axis 0)
    Given a matrix as STDIN
      """
      1 0 1 0 1
      0 1 0 1 0
      2 0 1 0 1
      0 2 0 1 0
      2 0 2 0 1
      """
    Given the file matrix1 containing
      """
      1 0 1 0 1
      0 1 0 1 0
      2 0 1 0 1
      0 2 0 1 0
      2 0 2 0 1
      """
    Given parameter --axis = 0
    Given the placeholder -
    Given file matrix1 as parameter
    When we run concatenate from manipulation with STDIN
    Then we expect the matrix
      """
      1 0 1 0 1
      0 1 0 1 0
      2 0 1 0 1
      0 2 0 1 0
      2 0 2 0 1
      1 0 1 0 1
      0 1 0 1 0
      2 0 1 0 1
      0 2 0 1 0
      2 0 2 0 1
      """


  @slice
  Scenario: run a test for keep-cols for slice
    Given a matrix as STDIN
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter --keep-cols = 3
    Given the placeholder -
    When we run vec_slice from manipulation with STDIN
    Then we expect the matrix
    """
    0
    1
    0
    1
    0
    """


  @slice
  Scenario: check params for slice with keep-cols
    Given the file test containing
    """
    id,c,d,e
    a,1,2,3
    b,3,4,5
    """
    Given file test as parameter
    Given parameter --keep-cols = 0,2
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    When we run vec_slice from manipulation with tmpfile
    Then we expect the matrix
    """
    id,c,e
    a,1,3
    b,3,5
    """


  @slice
  Scenario: check params for slice with keep-cols only column-titles
    Given the file test containing
    """
    c,d,e
    1,2,3
    3,4,5
    """
    Given file test as parameter
    Given parameter --keep-cols = 0,2
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    When we run vec_slice from manipulation with tmpfile
    Then we expect the matrix
    """
    c,e
    1,3
    3,5
    """


  @slice
  Scenario: check params for slice with keep-cols only row-titles
    Given the file test containing
    """
    a,1,2,3
    b,3,4,5
    """
    Given file test as parameter
    Given parameter --keep-cols = 0,2
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    When we run vec_slice from manipulation with tmpfile
    Then we expect the matrix
    """
    a,1,3
    b,3,5
    """


  @slice
  Scenario: run a test for remove-cols for slice
    Given a matrix as STDIN
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter --remove-cols = 0,1,2,4
    Given the placeholder -
    When we run vec_slice from manipulation with STDIN
    Then we expect the matrix
    """
    0
    1
    0
    1
    0
    """

  @slice
  Scenario: run a test for slice when both keep-cols and remove-cols are present
    Given a matrix as STDIN

    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter --keep-cols = 3
    Given parameter --remove-cols = 3
    Given the placeholder -
    When we run vec_slice from manipulation with STDIN
    Then we expect the matrix
    """
    0
    1
    0
    1
    0
    """


  Scenario: run a test for chop - chopping last row and third
    Given a int matrix
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter --remove-rows = 2,4,2
    When we run chop from manipulation
    Then we expect the matrix
    """
    1 0 1 0 1
    0 1 0 1 0
    0 2 0 1 0
    """

  Scenario: run a test for chop - what if keep-rows and remove-rows are defined?
    Given a int matrix
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter --keep-rows = 2,4,2
    Given parameter --remove-rows = 2,4,2
    When we run chop from manipulation
    Then we expect the matrix
    """
    2	0	1	0	1
    2 0 2 0 1
    2	0	1	0	1
    """

  Scenario: check params for chop with keep-rows for both row-titles and column-titles
    Given the file test containing
    """
    id,c,d,e
    a,1,2,3
    b,3,4,5
    c,1,1,0
    """
    Given file test as parameter
    Given parameter --keep-rows = 0,2
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    Given last parameter --column-titles
    When we run chop from manipulation with tmpfile
    Then we expect the matrix
    """
    id,c,d,e
    a,1,2,3
    c,1,1,0
    """

  Scenario: check params for chop with keep-rows for column-titles
    Given the file test containing
    """
    c,d,e
    1,2,3
    3,4,5
    1,1,0
    """
    Given file test as parameter
    Given parameter --keep-rows = 0,2
    Given parameter --delimiter = ,
    Given last parameter --column-titles
    When we run chop from manipulation with tmpfile
    Then we expect the matrix
    """
    c,d,e
    1,2,3
    1,1,0
    """


  Scenario: check params for chop with keep-rows for row-titles
    Given the file test containing
    """
    a,1,2,3
    b,3,4,5
    c,1,1,0
    """
    Given file test as parameter
    Given parameter --keep-rows = 0,2
    Given parameter --delimiter = ,
    Given last parameter --row-titles
    When we run chop from manipulation with tmpfile
    Then we expect the matrix
    """
    a,1,2,3
    c,1,1,0
    """


  Scenario: run a test for chop - chopping last row
    Given a int matrix
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter --remove-rows = 4
    When we run chop from manipulation
    Then we expect the matrix
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    """


  Scenario: run a test for chop - keeping first row
    Given a int matrix
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter --keep-rows = 0
    When we run chop from manipulation
    Then we expect the matrix
    """
    1 0 1 0 1
    """


  Scenario: run a test for chop - chopping range
    Given a int matrix
    """
    1 0 1 0 1
    0 1 0 1 0
    2 0 1 0 1
    0 2 0 1 0
    2 0 2 0 1
    """
    Given parameter --remove-rows = 1:3
    When we run chop from manipulation
    Then we expect the matrix
    """
    1 0 1 0 1
    2 0 2 0 1
    """



  Scenario: add values to matrix (default values)
    Given the file test containing
    """
    0,1,2
    1,2,1
    """
    Given file test as parameter
    Given parameter --values = 0
    Given parameter --delimiter = ,
    When we run append_values_to from manipulation with tmpfile
    Then we expect the matrix
    """
    0,1,2,0
    1,2,1,0
    """


  Scenario: add values to matrix (prepend row with zeros)
    Given the file test containing
    """
    id,a,b
    0,1,2
    1,2,1
    """
    Given last parameter -cr
    Given parameter --delimiter = ,
    Given file test as parameter
    Given parameter --values = 0
    Given parameter --to = 0
    Given parameter --axis = 0
    When we run append_values_to from manipulation with tmpfile
    Then we expect the matrix
    """
    id,a,b
    r0,0,0
    0,1,2
    1,2,1
    """

  Scenario: add values to matrix (append column with protein_coding)
    Given the file test containing
    """
    id,a,b
    0,1,2
    1,2,1
    """
    Given last parameter -cr
    Given parameter --delimiter = ,
    Given file test as parameter
    Given parameter --values = protein_coding
    Given parameter --to = -1
    Given parameter --axis = 1
    When we run append_values_to from manipulation with tmpfile
    Then we expect the matrix
    """
    id,a,b,c0
    0,1,2,protein_coding
    1,2,1,protein_coding
    """

  Scenario: add values to matrix (append multiple columns without position)
    Given the file test containing
    """
    id,a,b
    0,1,2
    1,2,1
    """
    Given last parameter -cr
    Given file test as parameter
    Given parameter --delimiter = ,
    Given parameter --values = a,b,c
    Given parameter --axis = 1
    When we run append_values_to from manipulation with tmpfile
    Then we expect the matrix
    """
    id,a,b,c0,c1,c2
    0,1,2,a,b,c
    1,2,1,a,b,c
    """

  Scenario: add values to matrix (append multiple columns with position)
    Given the file test containing
    """
    id,a,b
    0,1,2
    1,2,1
    """
    Given last parameter -cr
    Given file test as parameter
    Given parameter --values = a,b,c
    Given parameter --delimiter = ,
    Given parameter --to = 0,1,-1
    Given parameter --axis = 1
    When we run append_values_to from manipulation with tmpfile
    Then we expect the matrix
    """
    id,c0,c1,a,b,c2
    0,a,b,1,2,c
    1,a,b,2,1,c
    """


  Scenario: add values to matrix (append multiple columns with the same position)
    Given the file test containing
    """
    id,a,b
    0,1,2
    1,2,1
    """
    Given last parameter -cr
    Given file test as parameter
    Given parameter --delimiter = ,
    Given parameter --values = a,b,c
    Given parameter --to = 0,0,0
    Given parameter --axis = 1
    When we run append_values_to from manipulation with tmpfile
    Then we expect the matrix
    """
    id,c2,c1,c0,a,b
    0,c,b,a,1,2
    1,c,b,a,2,1
    """

  @format
  Scenario: change format from csv to svmlight
    Given the file test containing
    """
    2,50,12500,98,1
    0,13,3250,28,1
    1,16,4000,35,1
    2,20,5000,45,1
    1,24,6000,77,-1
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given parameter --class-column = 4
    Given parameter --in-format = csv
    Given parameter --out-format = svmlight
    When we run format_vec from manipulation with tmpfile
    Then we expect the matrix
    """
    1,1:2,2:50,3:12500,4:98
    1,1:0,2:13,3:3250,4:28
    1,1:1,2:16,3:4000,4:35
    1,1:2,2:20,3:5000,4:45
    -1,1:1,2:24,3:6000,4:77
    """

  @format
  Scenario: change format from csv to named svmlight
    Given the file test containing
    """
    a,b,c,d,class
    2,50,12500,98,1
    0,13,3250,28,1
    1,16,4000,35,1
    2,20,5000,45,1
    1,24,6000,77,-1
    """
    Given file test as parameter
    Given parameter --delimiter = ,
    Given parameter --class-column = 4
    Given parameter --in-format = csv
    Given parameter --out-format = svmlight
    Given last parameter --named
    Given last parameter --column-titles
    When we run format_vec from manipulation with tmpfile
    Then we expect the matrix
    """
    1,a:2,b:50,c:12500,d:98
    1,a:0,b:13,c:3250,d:28
    1,a:1,b:16,c:4000,d:35
    1,a:2,b:20,c:5000,d:45
    -1,a:1,b:24,c:6000,d:77
    """

  @format
  Scenario: change format from csv to svmlight with default params
    Given the file test containing
    """
    1	2	50	12500	98
    1	0	13	3250	28
    1	1	16	4000	35
    1	2	20	5000	45
    -1	1	24	6000	77
    """
    Given file test as parameter
    When we run format_vec from manipulation with tmpfile
    Then we expect the matrix
    """
    1	1:2	2:50	3:12500	4:98
    1	1:0	2:13	3:3250	4:28
    1	1:1	2:16	3:4000	4:35
    1	1:2	2:20	3:5000	4:45
    -1	1:1	2:24	3:6000	4:77
    """

  @format
  Scenario: change format from svmlight to csv with default params
    Given the file test containing
      """
      1	1:2	2:50	3:12500	4:98
      1	1:0	2:13	3:3250	4:28
      1	1:1	2:16	3:4000	4:35
      1	1:2	2:20	3:5000	4:45
      -1	1:1	2:24	3:6000	4:77
      """
    Given file test as parameter
    Given parameter --in-format = svmlight
    Given parameter --out-format = csv
    When we run format_vec from manipulation with tmpfile
    Then we expect the matrix
    """
    1	2	50	12500	98
    1	0	13	3250	28
    1	1	16	4000	35
    1	2	20	5000	45
    -1	1	24	6000	77
    """

  @format
  Scenario: change format from svmlight to csv with non default params
    Given the file test containing
    """
    1	1:2	2:50	3:12500	4:98
    1	1:0	2:13	3:3250	4:28
    1	1:1	2:16	3:4 5:4000	4:35
    1	1:2	2:20	3:5000	4:45
    -1	1:1	2:24	3:6000	4:77
      """
    Given file test as parameter
    Given last parameter --named
    Given parameter --in-format = svmlight
    Given parameter --out-format = csv
    When we run format_vec from manipulation with tmpfile
    Then we expect the matrix
    """
    Class 1 2 3 4 5
    1	2	50	12500	98  --
    1	0	13	3250	28  --
    1	1	16	4 35 4000
    1	2	20	5000	45  --
    -1	1	24	6000	77  --
    """

  @format
  Scenario: change format from svmlight to csv with non default params
    Given the file test containing
      """
      1	1:2	2:hallo	3:4
      -1	abc:3	4:3	3:2	2:1	0:1
      """
    Given file test as parameter
    Given last parameter --named
    Given parameter --in-format = svmlight
    Given parameter --out-format = csv
    When we run format_vec from manipulation with tmpfile
    Then we expect the matrix
    """
    Class 1 2 3 abc 4 0
    1 2 hallo 4 --  --  --
    -1  --  1 2 3 3 1
    """



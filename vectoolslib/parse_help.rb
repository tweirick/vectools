#!/usr/bin/env ruby
class VecDocParse
  def initialize  
    vec_help =  `vectortools.py`

    vec_help_splitted = vec_help.split("\n").map { |x| x.gsub(/^\s+/, "")}

    @file_name = ""
    @functions = []

    vec_help_splitted.each do |line|
      unless line.include? "\t"
        unless @functions.empty?
          parse_functions
          @functions = []
        end  
        @file_name = line
      else
        @functions << line
      end
    end
  end

  def parse_functions
    file = "#{@file_name}.md"
    unless File.exist? file
      File.open(file, "w") do |f|
        f.write(@file_name)
        puts " * [[#{@file_name}]]"
        f.write("\n")
        f.write("="*@file_name.length)
        f.write("\n")
        f.write("\n")
        f.write("##Functions:##\n")
        @functions.each do |fun|
          fun_a = fun.split("\t")
          f.write(" * [#{fun_a[0]}](#markdown-header-#{fun_a[0]})\n")
          puts(" "*4+" * [#{fun_a[0]}](#{@file_name}#markdown-header-#{fun_a[0]})\n")
        end
        f.write("\n")
        f.write("\n")
        @functions.each do |fun|
          fun_a = fun.split("\t-")
          f.write("####{fun_a[0]}###\n")
          f.write("#####{fun_a[1]}####\n")
          help = `vectortools.py #{fun_a[0]} --help`
          f.write("```bash\n")
          help.split("\n").each do |line|
            f.write(line+"\n")
          end
          f.write("```\n")
          #f.write(help)
          f.write("***\n")
        end
      end

#    {|f| f.write("write your stuff here") }

    else 
      STDERR.puts "file #{file} already exists. Please delete it first"
    end

  end

end

VecDocParse.new

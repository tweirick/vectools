import unittest
from vectoolslib import utilities
# @TODO: https://docs.python.org/3/library/doctest.html#module-doctest


def test_values(expected_value, test_value):
    """

    :param expected_value:
    :param test_value:
    :return:
    """
    test_result = expected_value == test_value

    if not test_result:
        print(expected_value, "!=", test_value)
        print(type(expected_value))
        print(type(test_value))

    return test_result


class UtilitiesTests(unittest.TestCase):
    # ========================================================================
    # Start pythonic_coordinates_to_exact_coordinates() tests
    # (slice_str, row_len, split_char=",", range_char=":")
    # return list - containing coordinates
    # ========================================================================

    def test__get_a_simple_first_coordinate(self):
        test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        list_length = len(test_list)
        coordinate_str = "0"
        expected_output = [0]
        function_output = utilities.pythonic_coordinates_to_exact_coordinates_list(coordinate_str, list_length)
        self.assertTrue(test_values(function_output, expected_output))

    def test__get_a_simple_last_coordinate(self):
        test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        list_length = len(test_list)
        coordinate_str = "9"
        expected_output = [9]
        function_output = utilities.pythonic_coordinates_to_exact_coordinates_list(coordinate_str, list_length)
        self.assertTrue(test_values(function_output, expected_output))

    def test__get_a_simple_coordinate_via_negative_index(self):
        test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        list_length = len(test_list)
        coordinate_str = "-1"
        expected_output = [9]
        function_output = utilities.pythonic_coordinates_to_exact_coordinates_list(coordinate_str, list_length)
        self.assertTrue(test_values(function_output, expected_output))

    def test__get_a_coordinates_from_slice(self):
        test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        list_length = len(test_list)
        coordinate_str = "2:5"
        expected_output = [2, 3, 4]
        function_output = utilities.pythonic_coordinates_to_exact_coordinates_list(coordinate_str, list_length)
        self.assertTrue(test_values(function_output, expected_output))

    def test__get_a_coordinates_from_slice_with_one_negative_index(self):
        test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        list_length = len(test_list)
        coordinate_str = "2:-3"
        expected_output = [2, 3, 4, 5, 6]
        function_output = utilities.pythonic_coordinates_to_exact_coordinates_list(coordinate_str, list_length)
        self.assertTrue(test_values(function_output, expected_output))

    def test__get_a_coordinates_from_slice_with_single_coordinate_and_slice(self):
        test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        list_length = len(test_list)
        coordinate_str = "0,3:-3"
        expected_output = [0, 3, 4, 5, 6]
        function_output = utilities.pythonic_coordinates_to_exact_coordinates_list(coordinate_str, list_length)
        self.assertTrue(test_values(function_output, expected_output))

    def test__get_a_coordinates_from_slice_with_single_coordinate_and_reversed_slice(self):
        test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        list_length = len(test_list)
        coordinate_str = "0,-3:3"
        expected_output = [0, 6, 5, 4, 3]
        function_output = utilities.pythonic_coordinates_to_exact_coordinates_list(coordinate_str, list_length)
        self.assertTrue(test_values(function_output, expected_output))

if __name__ == '__main__':
    unittest.main()

Vectools was created to simultaneously simplify and increase the development speed of informatics pipelines by
reducing the amount of custom programs needed in analyses. Furthermore, Vectools is extensively tested reducing the
changes of bungs in pipelines.

Vectools offers a wide range of operations related to the manipulation of vectors an matrices.
These operations include:
- Linear algebra
- Table (Spreadsheets, CSV/TSV files, etc.) manipulation (Join, uniquifying, )
- Machine learning (Supervised and Unsupervised)
- Support functions for machine learning (descriptors, PCA, feature selection, normalization, etc.).
- Statistical analysis
- Network-Graph manipulation

